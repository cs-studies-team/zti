# To check:
#   - rdflib
#   - Python: Writing to / reading from a file
#   - Python: Accesing iteration step
#   - sparqlwrapper

# Wejście: Lista zdań w j. angielskim, oddzielonych enterami w pliku `input.txt`.
# Spodziewany rezultat: plik `output.ttl`, zawierający dane w formacie takim
#                       jak w `sample_output.ttl`.

# 0. Import required modules.
from rdflib import URIRef, Graph, Literal, Namespace
from rdflib.namespace import XSD, RDF, RDFS

import entity_parser
import sparql_worker

# 1. Create a graph.
graph = Graph()

# 2. Insert the `dbo:` and `ex:` prefixes into the graph.
DBO = Namespace('http://dbpedia.org/ontology/')
EX = Namespace("http://example.org/")
graph.bind('dbo', DBO)
graph.bind('ex', EX)

# 3. For every newline-delimited string from "input.txt":
input = open('input.txt')

for number, sentence in enumerate(input.readlines(), 1):

#     3.1. Break the string down into noun phrases / entities
#          and save it as a new local list variable.

    phrases = entity_parser.parseText(sentence)

    print(phrases)

#     3.2. Insert two new triples into the graph:

#            + ex:Sentence_{iteration_step}    a            rdf:Seq .
#            + ex:Sentence_{iteration_step}    rdf:value    "{iteration_string}"@en .

    sentence_node = URIRef(f'http://example.org/Sentence_{number}')

    graph.add((sentence_node, RDF.type, RDF.Seq))
    graph.add((sentence_node, RDF.value, Literal(sentence, lang='en')))

#     3.3. For every element in this list:

    for number2, phrase in enumerate(phrases, 1):

#          3.3.1. Insert two new triples into the graph:

#                 + ex:Sentence_{iteration_step}
#                   rdf:_{subiteration_step}
#                   ex:Sentence_{iteration_step}_Phrase_{subiteration_step} .

#                 + ex:Sentence_{iteration_step}_Phrase_{subiteration_step}
#                   rdf:value
#                   "{subiteration_string}"@en .

        phrase_node = URIRef(f'http://example.org/Sentence_{number}_Phrase_{number2}')
        rdf_seq_node = URIRef(f'http://www.w3.org/1999/02/22-rdf-syntax-ns#_{number2}')

        graph.add((sentence_node, rdf_seq_node, phrase_node))
        graph.add((phrase_node, RDF.value, Literal(phrase, lang='en')))

#          3.3.2. Get all DBpedia classes for the phrase.

        dbclasses = sparql_worker.get_dbpedia_classes(phrase)

#                 a. For every identified class:

        for dbclass in dbclasses:

#                    i. Add a triple to the graph:

#                       + ex:Sentence_{iteration_step}_Phrase_{subiteration_step}
#                         a
#                         {class}

            graph.add((phrase_node, RDF.type, URIRef(dbclass)))

# 4. Close 'input.txt'.

input.close()

# 5. Serialize the graph into "output.ttl", in Turtle format.

graph.serialize(destination="output.ttl")