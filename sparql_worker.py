# Funkcja get_dbpedia_classes(phrase):
#       Wejście: string `phrase`
#       Wyjście: lista stringów `dbclasses`

from SPARQLWrapper import SPARQLWrapper2

dbpedia = SPARQLWrapper2('http://dbpedia.org/sparql')

print('Module \'sparql_worker.py\' initialized!')

def get_dbpedia_classes(phrase: str):

    dbclasses = []

# Query dbpedia using the following pattern:

    dbpedia.setQuery('''
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

        SELECT DISTINCT ?class ?superclass {

            ?entity     rdfs:label          \"'''+phrase+'''\"@en ;
                        a                   ?class .
            ?class      rdfs:subClassOf*    ?superclass

            FILTER (STR(?superclass) = STR(dbo:Person)  ||
                    STR(?superclass) = STR(dbo:Place) ||
                    STR(?superclass) = STR(dbo:Organisation))
        }''')

# Save the result as string list.

    for result in dbpedia.query().bindings:
        
        dbclasses.append(result['class'].value)

    return dbclasses