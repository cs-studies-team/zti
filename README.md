# Zastosowania Technologii Informatycznych (projekt), 2022


Politechnika Poznańska  
Informatyka (WE), niestacj. I st.  
8\. semestr 

## Autorzy
Jakub Więcek, nr albumu: 142122  
Szymon Wiśniewski, nr albumu: 130845  
Piotr Zajdziński, nr albumu: 131854

## Status projektu
Zakończony.

## Opis
Celem projektu było stworzenie aplikacji, która realizuje zadanie 1 z OKE 2018 (_Focused Named Entity Identification and Linking_), czyli która w wejściowym tekście w języku angielskim rozpoznaje imiona / nazwy własne, przypisuje je do odpowiednich klas z ontologii DBpedii, spośród `dbo:Person`, `dbo:Place`, `dbo:Organisation` oraz ich podklas, a jako wyjście produkuje RDF w dowolnym formacie (oczywiście najlepiej Turtle).

## Dane wejściowe
Lista zdań w języku angielskim, oddzielonych znakiem nowej linii (potocznie _"Enter"_) w pliku `input.txt`.

## Dane wyjściowe
Graf RDF w formie pliku `.ttl`, zestawiający:
* każde z wejściowych zdań,
* rozpoznane w nich nazwy własne / imiona,
* przypisane tym nazwom klasy `dbo:Person`, `dbo.Place`, `dbo:Organisation` i/lub ich podklasy.

Wzór danych wyjściowych znajduje się w pliku `sample_output.ttl`.

## Algorytm aplikacji

1. Stwórz i zainicjalizuj graf.
2. Załaduj listę zdań z pliku `input.txt`.
3. Dla każdego takiego zdania (`i` spośród `n`):
   1. dodaj do grafu dwie trójki:
      * `ex:Sentence_i, rdf:type, rdf:Seq`
      * `ex:Sentence_i, rdf:value, "zdanie"@en`
   2. rozbij zdanie na listę nazw własnych / imion, zwanych dalej frazami,
   3. dla każdej takiej frazy (`j` spośród `m`):
      1. dodaj do grafu dwie trójki:
         * `ex:Sentence_i, rdf:_j, ex:Sentence_i_Phrase_j`
         * `ex:Sentence_i_Phrase_j, rdf:value, "fraza"@en`
      2. wyślij poniższe zapytanie SPARQL do DBpedii:
      ```
      PREFIX dbo: <http://dbpedia.org/ontology/>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

      SELECT DISTINCT ?class ?superclass {

         ?entity     rdfs:label          "fraza"@en ;
                     a                   ?class .
         ?class      rdfs:subClassOf*    ?superclass

         FILTER (STR(?superclass) = STR(dbo:Person)  ||
                  STR(?superclass) = STR(dbo:Place) ||
                  STR(?superclass) = STR(dbo:Organisation))
      ```
      3. z odpowiedzi wyłuskaj listę identyfikatorów IRI klas (zmienna `?class`),
      4. dla każdego IRI z tej listy:
         1. dodaj do grafu trójkę:
         * `ex:Sentence_i_Phrase_j, rdf:type, IRI`
4. Zapisz graf w formacie Turtle, jako plik `output.ttl`.

## Wykorzystane narzędzia

#### Programowanie

* Python +
   * `spaCy` -- biblioteka do przetwarzania języka naturalnego,
   * `rdflib` -- biblioteka do pracy z RDF,
   * `sparqlwrapper` -- biblioteka do pracy ze SPARQL.

#### Praca grupowa

* Git
* GitLab (instancja Salsa, udostępniona przez organizację Debiana):
   * zdalne repozytorium git
   * planowanie pracy za pomocą issues
   * koordynacja pracy za pomocą merge requests
* Jitsi Meet (spotkania wideo)
* Meta (Facebook) Messenger

## Struktura aplikacji

* Moduł `entity_parser.py`
   * przetworzenie zdania na listę fraz.
* Moduł `sparql_worker.py`
   * przetworzenie frazy na listę pasujących do niej klas z DBpedii.
* Moduł `controller.py`
   * stworzenie grafu,
   * iteracyjne przetworzenie wejściowej listy zdań, a także pośrednich list fraz dla każdego z nich, za pomocą pozostałych modułów,
   * uzupełnianie grafu,
   * serializacja grafu do pliku `.ttl`.

## Podział pracy

#### Szymon Wiśniewski

* Moduł 2 -- `sparql_worker.py`
* Moduł 3 -- `controller.py`

#### Jakub Więcek

* Moduł 1 -- `entity_parser.py`

#### Piotr Zajdziński

* Dokumentacja

## Instalacja
1. Sklonuj niniejsze repozytorium.
2. Zainstaluj Pythona 3.
3. Uruchom konsolę systemową, przejdź do folderu z repozytorium i stwórz nowe środowisko wirtualne Pythona, poleceniem:  
   `python3 -m venv ./`  
   Następnie aktywuj to środowisko poleceniem:  
   `source ./bin/activate`  
4. Zainstaluj zależności poleceniami:  
   * `pip install rdflib sparqlwrapper setuptools wheel spacy`  
   * `spacy download en_core_web_sm`  

## Uruchomienie
1. Umieść dane wejściowe w pliku `input.txt`. Muszą to być zdania w języku angielskim, oddzielone znakiem nowej linii (potocznie _"Enterem"_ == `\n`).
2. Uruchom plik `controller.py` w wybrany sposób:
   * z konsoli systemowej za pomocą polecenia `python3 /scieżka/do/controller.py`,
   * z poziomu interfejsu graficznego swojego IDE.
3. Dane wyjściowe znajdują się w wygenerowanym pliku `output.ttl`.

## Podsumowanie

Cel został w większości osiągnięty.

Niektórych nazw własnych / imion aplikacja nie rozpoznaje, ale wynika to zazwyczaj z niespójności hierarchii klas w DBpedii (rozpoznana nazwa pasuje do odpowiedniej klasy, ale nie jest ona podklasą żadnej z 3 wymienionych w poprzednim akapicie, choć powinna być; dobry przykład to _OECD_ z przykładowych danych wejściowych), rzadziej z niedoskonałego działania modułu NLP (np. niepożądane uwzględnienie _the_ w nazwie _"the United Nations"_, co skutkuje niepowiązaniem jej przez zapytanie SPARQL z pożądaną klasą _"United Nations"_ z DBpedii).