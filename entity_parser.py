import spacy

# Wybranie odpowiednich "labels"
labels = ['PERSON', 'GPE', 'ORG']

# Załadowanie modelu dla zaistalowanej paczki językowej
nlp = spacy.load("en_core_web_sm")

print("Module \'entity_parser.py\' initialized!")

def parseText(text: str):
     """
          Przetworzenie tekstu wejściowego
          
          text (str): Tekst wejściowy.
          RETURNS (Array): Tablica wykrytych wyrazów.
     """
     result = []
     doc = nlp(text)

     # Iteracja po wykrytych wyrazach
     for ent in doc.ents:

          # Sprawdzenie czy etykieta znajduje się w liście
          if(ent.label_ in labels): 

               # dodanie wyrazu do zwracanej tablicy
               result.append(ent.text)
               
     return result